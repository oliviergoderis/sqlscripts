USE ModernWays;
ALTER TABLE Liedjes ADD Genre VARCHAR(20);
SET SQL_SAFE_UPDATES = 0;
UPDATE Liedjes
SET Genre = 'Hard Rock'
WHERE 	Artiest = 'Led Zeppelin' OR 
		Artiest = 'Van Halen' ;
SET SQL_SAFE_UPDATES = 1;