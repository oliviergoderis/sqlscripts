USE ModernWays;
ALTER TABLE Huisdieren ADD Geluid VARCHAR(20);
SET SQL_SAFE_UPDATES = 0;
UPDATE Huisdieren
SET Geluid = 'WAF'
WHERE Soort = 'Hond';
UPDATE huisdieren
SET Geluid = "miauwww..."
WHERE Soort = 'kat';
SET SQL_SAFE_UPDATES = 1;